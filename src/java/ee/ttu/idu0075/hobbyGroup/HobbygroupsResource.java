/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.ttu.idu0075.hobbyGroup;

import ee.ttu.idu0075._2015.ws.hobbygroup.AddHobbyGroupRequest;
import ee.ttu.idu0075._2015.ws.hobbygroup.AddHobbyGroupStudentRequest;
import ee.ttu.idu0075._2015.ws.hobbygroup.GetHobbyGroupListRequest;
import ee.ttu.idu0075._2015.ws.hobbygroup.GetHobbyGroupListResponse;
import ee.ttu.idu0075._2015.ws.hobbygroup.GetHobbyGroupRequest;
import ee.ttu.idu0075._2015.ws.hobbygroup.GetHobbyGroupStudentListRequest;
import ee.ttu.idu0075._2015.ws.hobbygroup.HobbyGroupStudentListType;
import ee.ttu.idu0075._2015.ws.hobbygroup.HobbyGroupStudentType;
import ee.ttu.idu0075._2015.ws.hobbygroup.HobbyGroupType;
import ee.ttu.idu0075._2015.ws.hobbygroup.LessonType;
import java.math.BigInteger;
import java.util.List;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author Juhan
 */
@Path("hobbygroups")
public class HobbygroupsResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of HobbygroupsResource
     */
    public HobbygroupsResource() {
    }

    @GET
    @Produces("application/json")
    public GetHobbyGroupListResponse getHobbyGroupList(@QueryParam("token") String token) {
        HobbyGroupService ws = new HobbyGroupService();
        GetHobbyGroupListRequest request = new GetHobbyGroupListRequest();
        request.setToken(token);
        return ws.getHobbyGroupList(request);
    } 
    
    @GET
    @Path("{id: \\d+}") //supports digits only
    @Produces("application/json")
    public HobbyGroupType getHobbyGroup(@PathParam("id") String id,
            @QueryParam("token") String token) {
        HobbyGroupService ws = new HobbyGroupService();
        GetHobbyGroupRequest request = new GetHobbyGroupRequest();
        request.setId(BigInteger.valueOf(Integer.parseInt(id)));
        request.setToken(token);
        return ws.getHobbyGroup(request);
    }
    
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public HobbyGroupType addHobbyGroup(HobbyGroupType content,
            @QueryParam("token") String token) {
        HobbyGroupService ws = new HobbyGroupService();
        AddHobbyGroupRequest request = new AddHobbyGroupRequest();
        request.setHobbyGroupName(content.getName());
        List<AddHobbyGroupRequest.Lesson> lessons = request.getLesson();
        for (LessonType lt : content.getLesson()) {
            AddHobbyGroupRequest.Lesson lesson = new AddHobbyGroupRequest.Lesson();
            lesson.setDay(lt.getDay());
            lesson.setStartTime(lt.getStartTime());
            lesson.setEndTime(lt.getEndTime());
            lesson.setInstructor(lt.getInstructor());
            lesson.setLocation(lt.getLocation());
            lessons.add(lesson);        
        }
        request.setMonthlyPrice(content.getMonthlyPrice());
        request.setToken(token);
        return ws.addHobbyGroup(request);
    }
    
    @GET
    @Path("{id: \\d+}/students") //supports digits only
    @Produces("application/json")
    public HobbyGroupStudentListType getHobbyGroupStudentList(@PathParam("id") String id,
            @QueryParam("token") String token) {
        HobbyGroupService ws = new HobbyGroupService();
        GetHobbyGroupStudentListRequest request = new GetHobbyGroupStudentListRequest();
        request.setHobbyGroupId(BigInteger.valueOf(Integer.parseInt(id)));
        request.setToken(token);
        return ws.getHobbyGroupStudentList(request);
    }
    
    @POST
    @Path("{id: \\d+}/students")
    @Consumes("application/json")
    @Produces("application/json")
    public HobbyGroupStudentType addHobbyGroupStudent(HobbyGroupStudentType content,
            @PathParam("id") String id,
            @QueryParam("token") String token) {
        HobbyGroupService ws = new HobbyGroupService();
        AddHobbyGroupStudentRequest request = new AddHobbyGroupStudentRequest();
        request.setHobbyGroupId(new BigInteger(id));
        request.setStudentId(content.getStudent().getId());
        request.setToken(token);
        return ws.addHobbyGroupStudent(request);
    }
}
