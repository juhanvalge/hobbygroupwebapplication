/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.ttu.idu0075.hobbyGroup;

import ee.ttu.idu0075._2015.ws.hobbygroup.AddStudentRequest;
import ee.ttu.idu0075._2015.ws.hobbygroup.GetStudentListRequest;
import ee.ttu.idu0075._2015.ws.hobbygroup.GetStudentListResponse;
import ee.ttu.idu0075._2015.ws.hobbygroup.GetStudentRequest;
import ee.ttu.idu0075._2015.ws.hobbygroup.StudentType;
import java.math.BigInteger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author Juhan
 */
@Path("students")
public class StudentsResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of StudentsResource
     */
    public StudentsResource() {
    }

    @GET
    @Produces("application/json")
    public GetStudentListResponse getStudentList(@QueryParam("token") String token) {
        HobbyGroupService ws = new HobbyGroupService();
        GetStudentListRequest request = new GetStudentListRequest();
        request.setToken(token);
        return ws.getStudentList(request);
    } 
    
    @GET
    @Path("{id: [0-9]*}") //supports digits only
    @Produces("application/json")
    public StudentType getStudent(@PathParam("id") String id,
            @QueryParam("token") String token) {
        HobbyGroupService ws = new HobbyGroupService();
        GetStudentRequest request = new GetStudentRequest();
        request.setId(new BigInteger(id));
        request.setToken(token);
        return ws.getStudent(request);
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public StudentType addStudent(StudentType content,
            @QueryParam("token") String token) {
        HobbyGroupService ws = new HobbyGroupService();
        AddStudentRequest request = new AddStudentRequest();
        request.setIdCode(content.getIdCode());
        request.setName(content.getName());
        request.setToken(token);
        return ws.addStudent(request);
    }
}
