/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.ttu.idu0075.hobbyGroup;

import ee.ttu.idu0075._2015.ws.hobbygroup.GetStudentListRequest;
import ee.ttu.idu0075._2015.ws.hobbygroup.GetStudentListResponse;
import ee.ttu.idu0075._2015.ws.hobbygroup.HobbyGroupService;
import ee.ttu.idu0075._2015.ws.hobbygroup.StudentType;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.WebServiceRef;

/**
 *
 * @author Juhan
 */
@WebServlet(name = "GetStudentsServlet", urlPatterns = {"/GetStudentsServlet"})
public class GetStudentsServlet extends HttpServlet {

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/e7450_8080/HobbyGroupWebApplication/HobbyGroupService.wsdl")
    private HobbyGroupService service;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        
        
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String Token = request.getParameter("token");
            GetStudentListRequest gsltRequest = new GetStudentListRequest();
            gsltRequest.setToken(Token);
            GetStudentListResponse studentList = getStudentList(gsltRequest);

            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<meta charset='UTF-8'>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h2>Õpilased</h2>");
            if (studentList.getStudent().size() == 0) out.println("<p>Hetkel pole lisatud ühtegi õpilast</p>");
            for (StudentType student : studentList.getStudent()) {
                out.println(student.getName() + "<br>");
            }
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private GetStudentListResponse getStudentList(ee.ttu.idu0075._2015.ws.hobbygroup.GetStudentListRequest parameter) {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        ee.ttu.idu0075._2015.ws.hobbygroup.HobbyGroupPortType port = service.getHobbyGroupPort();
        return port.getStudentList(parameter);
    }

}
