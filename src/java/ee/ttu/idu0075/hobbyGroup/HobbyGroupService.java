/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ee.ttu.idu0075.hobbyGroup;

import ee.ttu.idu0075._2015.ws.hobbygroup.AddHobbyGroupRequest;
import ee.ttu.idu0075._2015.ws.hobbygroup.AddHobbyGroupStudentRequest;
import ee.ttu.idu0075._2015.ws.hobbygroup.AddStudentRequest;
import ee.ttu.idu0075._2015.ws.hobbygroup.GetHobbyGroupListRequest;
import ee.ttu.idu0075._2015.ws.hobbygroup.GetHobbyGroupListResponse;
import ee.ttu.idu0075._2015.ws.hobbygroup.GetHobbyGroupRequest;
import ee.ttu.idu0075._2015.ws.hobbygroup.GetHobbyGroupStudentListRequest;
import ee.ttu.idu0075._2015.ws.hobbygroup.GetStudentListRequest;
import ee.ttu.idu0075._2015.ws.hobbygroup.GetStudentListResponse;
import ee.ttu.idu0075._2015.ws.hobbygroup.GetStudentRequest;
import ee.ttu.idu0075._2015.ws.hobbygroup.HobbyGroupStudentListType;
import ee.ttu.idu0075._2015.ws.hobbygroup.HobbyGroupStudentType;
import ee.ttu.idu0075._2015.ws.hobbygroup.HobbyGroupType;
import ee.ttu.idu0075._2015.ws.hobbygroup.LessonType;
import ee.ttu.idu0075._2015.ws.hobbygroup.StudentType;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jws.WebService;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;

/**
 *
 * @author Juhan
 */
@WebService(serviceName = "HobbyGroupService", portName = "HobbyGroupPort", endpointInterface = "ee.ttu.idu0075._2015.ws.hobbygroup.HobbyGroupPortType", targetNamespace = "http://www.ttu.ee/idu0075/2015/ws/hobbyGroup", wsdlLocation = "WEB-INF/wsdl/HobbyGroupService/HobbyGroupService.wsdl")
public class HobbyGroupService {

    static int nextStudentId = 1;
    static List<StudentType> studentList = new ArrayList<StudentType>();
    static int nextHobbyGroupId = 1;
    static List<HobbyGroupType> hobbyGroupList = new ArrayList<HobbyGroupType>();
    static Map<BigInteger, HobbyGroupStudentListType> hobbyGroupStudentsMap = new HashMap<BigInteger, HobbyGroupStudentListType>();

    public StudentType getStudent(GetStudentRequest parameter) {
        tokenValidation(parameter.getToken());
        StudentType st = new StudentType();
        for (StudentType student : HobbyGroupService.studentList) {
            if (student.getId().equals(parameter.getId())) {
                st = student;
            }
        }
        return st;
    }

    public StudentType addStudent(AddStudentRequest parameter) {
        tokenValidation(parameter.getToken());
        StudentType st = getStudentWithTheseParameters(parameter);
        if (st.getId() == null) {
            st.setIdCode(parameter.getIdCode());
            st.setName(parameter.getName());
            st.setId(BigInteger.valueOf(nextStudentId++));
            studentList.add(st);
        }
        return st;
    }

    public GetStudentListResponse getStudentList(GetStudentListRequest parameter) {
        tokenValidation(parameter.getToken());
        GetStudentListResponse response = new GetStudentListResponse();
        for (StudentType student : studentList) {
            response.getStudent().add(student);
        }
        return response;
    }

    public HobbyGroupType getHobbyGroup(GetHobbyGroupRequest parameter) {
        tokenValidation(parameter.getToken());
        HobbyGroupType hgt = new HobbyGroupType();
        for (HobbyGroupType hobbyGroup : hobbyGroupList) {
            if (hobbyGroup.getId().equals(parameter.getId())) {
                hgt = hobbyGroup;
            }
        }
        return hgt;
    }

    public HobbyGroupType addHobbyGroup(AddHobbyGroupRequest parameter) {
        tokenValidation(parameter.getToken());
        HobbyGroupType hgt = getHobbyGroupWithTheseParameters(parameter);
        if (hgt.getId() == null) {
            hgt.setName(parameter.getHobbyGroupName());
            GregorianCalendar c = new GregorianCalendar();
            c.setTime(new Date());
            try {
                hgt.setRegistrationDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(c));
            } catch (DatatypeConfigurationException ex) {
                Logger.getLogger(HobbyGroupService.class.getName()).log(Level.SEVERE, null, ex);
            }
            List<AddHobbyGroupRequest.Lesson> requestLessons = parameter.getLesson();
            for (AddHobbyGroupRequest.Lesson requestLesson : requestLessons) {
                LessonType lesson = new LessonType();
                lesson.setDay(requestLesson.getDay());
                lesson.setStartTime(requestLesson.getStartTime());
                lesson.setEndTime(requestLesson.getEndTime());
                lesson.setInstructor(requestLesson.getInstructor());
                lesson.setLocation(requestLesson.getLocation());
                hgt.getLesson().add(lesson);
            }
            hgt.setMonthlyPrice(parameter.getMonthlyPrice());
            hgt.setId(BigInteger.valueOf(nextHobbyGroupId++));
            hobbyGroupList.add(hgt);
        }
        return hgt;
    }

    public GetHobbyGroupListResponse getHobbyGroupList(GetHobbyGroupListRequest parameter) {
        tokenValidation(parameter.getToken());
        GetHobbyGroupListResponse response = new GetHobbyGroupListResponse();
        for (HobbyGroupType hobbyGroup : hobbyGroupList) {
            if (parameter.getHobbyGroupName() == null 
                    || hobbyGroup.getName().toLowerCase().contains(parameter.getHobbyGroupName().toLowerCase())) {
            response.getHobbyGroup().add(hobbyGroup);
            }
        }
        return response;
    }

    public HobbyGroupStudentListType getHobbyGroupStudentList(GetHobbyGroupStudentListRequest parameter) {
        tokenValidation(parameter.getToken());
        if (hobbyGroupStudentsMap.containsKey(parameter.getHobbyGroupId())) {
            return hobbyGroupStudentsMap.get(parameter.getHobbyGroupId());
        }
        return new HobbyGroupStudentListType();
    }

    public HobbyGroupStudentType addHobbyGroupStudent(AddHobbyGroupStudentRequest parameter) {
        tokenValidation(parameter.getToken());
        HobbyGroupType hgt = getHobbyGroupFromList(parameter.getHobbyGroupId());
        StudentType st = getStudentFromList(parameter.getStudentId());
        HobbyGroupStudentType hgst = new HobbyGroupStudentType();
        if (hobbyGroupStudentsMap.containsKey(parameter.getHobbyGroupId())) {
            for (HobbyGroupStudentType hgstudent : hobbyGroupStudentsMap.get(parameter.getHobbyGroupId()).getHobbyGroupStudent()) {
                if (hgstudent.getStudent().getId().equals(parameter.getStudentId())) return hgstudent;
            }
            hgst = setHgstData(hgst, st);
            hobbyGroupStudentsMap.get(parameter.getHobbyGroupId()).getHobbyGroupStudent().add(hgst);
        } else {
            hgst = setHgstData(hgst, st);
            HobbyGroupStudentListType hgstList = new HobbyGroupStudentListType();
            hgstList.getHobbyGroupStudent().add(hgst);
            hobbyGroupStudentsMap.put(parameter.getHobbyGroupId(), hgstList);
        }
        return hgst;
    }
    
    private HobbyGroupStudentType setHgstData(HobbyGroupStudentType hgst, StudentType st) {
        hgst.setStudent(st);
        GregorianCalendar c = new GregorianCalendar();
        c.setTime(new Date());
        try {
            hgst.setMembershipStart(DatatypeFactory.newInstance().newXMLGregorianCalendar(c));
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(HobbyGroupService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return hgst;
    }
    
    private StudentType getStudentWithTheseParameters(AddStudentRequest parameter) {
        for (StudentType st : studentList) {
            if (st.getIdCode().equals(parameter.getIdCode()) && st.getName().equals(parameter.getName())) {
                return st;
            }
        }
        return new StudentType();
    }

    private HobbyGroupType getHobbyGroupWithTheseParameters(AddHobbyGroupRequest parameter) {
        for (HobbyGroupType hgt : hobbyGroupList) {
            if (hgt.getName().equalsIgnoreCase(parameter.getHobbyGroupName())) {
                return hgt;
            }
        }
        return new HobbyGroupType();
    }
    
    private void tokenValidation(String token) {
        if (token == null) throw new UnsupportedOperationException("Missing API token.");
        if (token.equalsIgnoreCase("salajane")) {
            return;
        }
        throw new UnsupportedOperationException("API token is not valid.");
    }
    
    private HobbyGroupType getHobbyGroupFromList(BigInteger hobbyGroupId) {
        for (HobbyGroupType hgt : hobbyGroupList) {
            if (hgt.getId().equals(hobbyGroupId)) {
                return hgt;
            }
        }
        throw new UnsupportedOperationException("There are no hobbygroups with this ID.");
    }
    
    private StudentType getStudentFromList(BigInteger studentId) {
        for (StudentType st : studentList) {
            if (st.getId().equals(studentId)) {
                return st;
            }
        }
        throw new UnsupportedOperationException("There are no students with this ID.");
    }
    
}
